import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
import numpy as np
from breast_cancer_decision_tree import breast_cancer_decision_tree
from breast_cancer_logistic_regression import breast_cancer_logistic_regression
from breast_cancer_knn import breast_cancer_knn
from teste import teste
from breast_cancer_k_means import breast_cancer_kmeans
from breast_cancer_rna import breast_cancer_rna
from breast_cancer_naive_bayes import breast_cancer_naive_bayes
from sklearn.datasets import load_breast_cancer
import joblib


X, y = load_breast_cancer(return_X_y = True)

features = ['radius','texture','perimeter','area','smoothness','compactness','concavity','concave points','symmetry','fractal dimension']
i = 0
#data_array=[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
data_array=[10.96,17.62,70.79,365.6,0.09687,0.09752,0.05263,0.02788,0.1619,0.06408,0.1507,1.583,1.165,10.09,0.009501,0.03378,0.04401,0.01346,0.01322,0.003534,11.62,26.51,76.43,407.5,0.1428,0.251,0.2123,0.09861,0.2289,0.08278]


# for i in range(10):
#     print("Insira, em ordem, os valores da média, erro e pior valor pra coluna "+features[i])
#     data_array[i]=input()
#     data_array[i+10]=input()
#     data_array[i+20]=input()

data = np.array(data_array).reshape((1, -1))
#data = (data2-np.amin(data2))/(np.amax(data2)-np.amin(data2))

logistic_regression = breast_cancer_logistic_regression()
knn = breast_cancer_knn()
rna = breast_cancer_rna()
teste = teste()
decision_tree = breast_cancer_decision_tree()
naive_bayes = breast_cancer_naive_bayes()
#
#train_log_regression = logistic_regression.treinar_dataset(X, y)
train_knn = knn.treinar_dataset(X, y)
# train_rna=rna.treinar_dataset(X, y)
train_tree= decision_tree.treinar_dataset(X, y)
train_nb = naive_bayes.treinar_dataset(X, y)
#train = teste.treinar_dataset(dataset)
#
result_knn = train_knn.predict(data)[0]
# result_rna = train_rna.predict(data)[0]
result_tree = train_tree.predict(data)[0]
result_nb = train_nb.predict(data)[0]
# result_log_regression = train_log_regression.predict(data)[0]
#result = train.predict(data)[0]

print("Resultado da naive bayes: {}".format(result_nb))
print("Resultado do knn: {}".format(result_knn))
# print("Resultado do RNA: {}".format(result_rna))
print("Resultado da árvore de decisão: {}".format(result_tree))
#print("Resultado do teste: {}".format(result))







# if(result_log_regression == 0 and result_knn==0):
#     print("O resultado é maligno")
# elif(result_log_regression==0 and result_rna==0):
#     print("O resultado é maligno")
# elif(result_knn==0 and result_rna==0):
#     print("O resultado é maligno")
# else:
#     print("O resultado é benigno")

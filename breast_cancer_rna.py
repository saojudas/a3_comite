
from normalizacao import normalizacao
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import MinMaxScaler
import numpy as np
from sklearn.metrics import classification_report
class breast_cancer_rna():
    final_rna = 0
    normalizacao = normalizacao()

    def treinar_dataset(self,X, y):
        final_accuracy = 0
        count_accuracy=0
        # self.dataset = dataset
        # normalized_dataset = normalizacao.normalizacao_dados(dataset)

         #MODELAGEM DOS DADOS
        scores = []
        i=0
        while i<100:
            #Separação entre dados de teste e de treinamento
            # X_train, X_test, y_train, y_test = train_test_split(normalized_dataset, dataset['target'], test_size=0.30)
            X = (X - np.amin(X)) / (np.amax(X) - np.amin(X))
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=1, shuffle=True)

            #Instanciando modelo
            rna = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5,5), random_state=2, max_iter=10000).fit(X_train, y_train)
            #Treinamento

            precisao = rna.score(X_test, y_test)
            scores.append(precisao)
            #Predições

            predictions = rna.predict(X_test)
            # print(predictions)
            #
            # print (confusion_matrix(y_test, predictions))
            # print(classification_report(y_test,predictions))
            accuracy = accuracy_score(y_test, predictions)
            if(accuracy>0.95):
                count_accuracy=count_accuracy+1
            if (accuracy > final_accuracy and accuracy<1.0):
                final_accuracy=accuracy

                final_rna = rna
            i = i+1

        print("Média do RNA: {:.2f}%".format(np.mean(scores)*100))
        print("Desvio padrão do RNA: {:.2f}%".format(np.std(scores) * 100))
        print("RNA - Normalizado com escalonamento de recurso min-max")
        print("Maior acurácia {}".format(final_accuracy))
        print("Qntd de vezes que a acurácia foi maior que 95% em 100 tentativas: {}\n".format(count_accuracy))
        #print("Acurácia do RNA: {}".format(final_accuracy))
        return final_rna


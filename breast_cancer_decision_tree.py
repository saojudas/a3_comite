
from sklearn import tree
import pandas as pd
from sklearn.metrics import classification_report
import numpy as np
from normalizacao import normalizacao
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler
class breast_cancer_decision_tree():
    normalizacao = normalizacao()
    final_clf = 0
    def treinar_dataset(self,X, y):
        final_accuracy = 0
        count_accuracy=0
        # dataset = dataset
        # normalized_dataset = normalizacao.normalizacao_dados(dataset)
        scores=[]
         #MODELAGEM DOS DADOS
        i=0
        while i<100:
            #Separação entre dados de teste e de treinamento
            # X_train, X_test, y_train, y_test = train_test_split(normalized_dataset, dataset['target'], test_size=0.30)

            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=1, shuffle=True)
            #Critérios de classificação
            clf = tree.DecisionTreeClassifier(criterion='entropy', max_depth=3)
            #Treinamento
            clf = clf.fit(X_train, y_train)
            precisao = clf.score(X_test, y_test)
            scores.append(precisao)
            #Predições

            predictions = clf.predict(X_test)
            #print(predictions)

            #print (confusion_matrix(y_test, predictions))
            #print(classification_report(y_test,predictions))
            accuracy = accuracy_score(y_test, predictions)
            if(accuracy>0.95):
                count_accuracy=count_accuracy+1
            if (accuracy > final_accuracy and accuracy<1.0):
                final_accuracy=accuracy
                final_clf = clf
            i = i+1
        print("Média da árvore de decisão : {:.2f}%".format(np.mean(scores)*100))
        print("Desvio padrão da árvore de decisão: {:.2f}%".format(np.std(scores) * 100))
        print("Árvore de decisão - Normalizado com escalonamento de recurso min-max")
        print("Maior acurácia {}".format(final_accuracy))
        print("Qntd de vezes que a acurácia foi maior que 95% em 100 tentativas: {} \n".format(count_accuracy))

        #print("Acurácia da árvore de decisão: {}".format(final_accuracy))

        return final_clf

        # resultado 0=n sobreviveu

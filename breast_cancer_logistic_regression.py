from sklearn.metrics import accuracy_score
import joblib
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from normalizacao import normalizacao

class breast_cancer_logistic_regression():
    final_logmodel = 0
    normalizacao = normalizacao()

    def treinar_dataset(self, X, y):
        final_accuracy = 0
        count_accuracy = 0
        # self.dataset = dataset
        # normalized_dataset = normalizacao.normalizacao_dados(dataset)
        scores =[]
        i = 0
        while i < 100:
            # Separação entre dados de teste e de treinamento
            # X_train, X_test, y_train, y_test = train_test_split(normalized_dataset, dataset['target'], test_size=0.30)
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=1, shuffle=True)

            #Aplicação do modelo
            logmodel = LogisticRegression(solver='saga')
            #Treinamento
            logmodel.fit(X_train, y_train)
            precisao = logmodel.score(X_test, y_test)
            scores.append(precisao)
            # Predições
            predictions = logmodel.predict(X_test)
            #
            #print(predictions)

            #print(confusion_matrix(y_test, predictions))
            #print(classification_report(y_test, predictions))
            accuracy = accuracy_score(y_test, predictions)
            if (accuracy > 0.95):
                count_accuracy = count_accuracy + 1
            if (accuracy > final_accuracy and accuracy<1.0):
                final_accuracy = accuracy
                final_logmodel = logmodel
            i = i + 1
        print("Média da regressão logística: {:.2f}%".format(np.mean(scores)*100))
        print("Desvio padrão da regressão logística: {:.2f}%".format(np.std(scores) * 100))
        print("Regressão logística - Normalizado com escalonamento de recurso min-max")
        print("Maior acurácia {}".format(final_accuracy))
        print("Qntd de vezes que a acurácia foi maior que 95% em 100 tentativas: {}\n".format(count_accuracy))

        return final_logmodel

    #def inferir(self, data):
        #return self.final_logmodel.predict(data)[0]
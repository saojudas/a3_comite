from sklearn.cluster import KMeans
import numpy as np
import pandas as pd
from normalizacao import normalizacao
from sklearn.datasets import load_breast_cancer
from scipy.stats import mode
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler

class breast_cancer_kmeans():
    final_kmeans=0
    normalizacao = normalizacao()

    def treinar_dataset(self,dataset):
        final_accuracy = 0
        count_accuracy = 0
        self.dataset = dataset
        normalized_dataset = normalizacao.normalizacao_dados(dataset)
        scores =[]
        n = 0
        while n < 100:
            kmeans = KMeans(n_clusters=2)
            clusters = kmeans.fit_predict(normalized_dataset.values)
            labels = np.zeros_like(clusters)
            for i in range(2):
                mask = (clusters ==i)
                labels[mask]=mode(dataset.target[mask])[0]
            #print(accuracy_score(dataset.target,labels))

            #
            #
            precisao = kmeans.score(dataset.target, labels)
            scores.append(precisao)
            accuracy = accuracy_score(dataset.target, labels)
            if (accuracy > 0.95):
                count_accuracy = count_accuracy + 1
            if (accuracy > final_accuracy and accuracy<1.0):
                final_accuracy = accuracy
                final_kmeans = kmeans

            n = n+1

        print("K-Means - Normalizado com escalonamento de recurso min-max")
        print("Maior acurácia {}".format(final_accuracy))
        print("Qntd de vezes que a acurácia foi maior que 95% em 100 tentativas: {}\n".format(count_accuracy))
        #print("Acurácia do kmeans: {}".format(final_accuracy))

        return final_kmeans

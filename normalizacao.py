import pandas as pd
from sklearn.preprocessing import MinMaxScaler

class normalizacao():

    #Método de normalização utilizando recurso min-max
    def normalizacao_dados(dataset):
        dataframe = pd.DataFrame(dataset['data'], columns=dataset['feature_names'])
        scaler_minMax = MinMaxScaler()
        data_normalized = pd.DataFrame(scaler_minMax.fit_transform(dataframe), columns=dataframe.columns)
        return data_normalized

        # # df_min_max_scaled = df.copy()
        # for column in dataframe.columns:
        #     dataframe[column] = (dataframe[column] - dataframe[column].min()) / (
        #                 dataframe[column].max() - dataframe[column].min())
        # return dataframe

    # Método de normalização utilizando escala máxima absoluta
    def normalizacao_dados_max(dataset):
        dataset = dataset
        dataframe = pd.DataFrame(dataset['data'], columns=dataset['feature_names'])
        for column in dataframe.columns:
            dataframe[column] = dataframe[column] / dataframe[column].abs().max()
        return dataframe

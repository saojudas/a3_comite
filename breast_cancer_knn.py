import pandas as pd
from sklearn.metrics import classification_report
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import confusion_matrix
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from normalizacao import normalizacao
class breast_cancer_knn():
    final_knn=0
    normalizacao = normalizacao()

    def treinar_dataset(self,X, y):
        final_accuracy = 0
        count_accuracy = 0
        # self.dataset = dataset
        # normalized_dataset = normalizacao.normalizacao_dados(dataset)
        scores =[]
        i = 0
        while i < 100:
            # Separação entre dados de teste e de treinamento
            #X_train, X_test, y_train, y_test = train_test_split(normalized_dataset, dataset['target'], test_size=0.30)
            #X = (X - np.amin(X)) / (np.amax(X) - np.amin(X))
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=1, shuffle=True)

            #Aplicação do modelo
            #knn = KNeighborsClassifier(n_neighbors=7, metric='euclidean')
            knn = KNeighborsClassifier(n_neighbors=12, weights='distance').fit(X_train, y_train)
            #Treinamento
            #knn.fit(X_train.values, y_train.values)
            precisao = knn.score(X_test, y_test)
            scores.append(precisao)
            #Predições
            predictions = knn.predict(X_test)

            #
            #print(confusion_matrix(y_test, predictions))

            accuracy = accuracy_score(y_test, predictions)
            if (accuracy > 0.95):
                count_accuracy = count_accuracy + 1
            if (accuracy > final_accuracy and accuracy<1.0):
                final_accuracy = accuracy

                final_knn = knn
            i = i + 1

        print("Média do KNN: {:.2f}%".format(np.mean(scores)*100))
        print("Desvio padrão do KNN: {:.2f}%".format(np.std(scores) * 100))
        print("KNN - Normalizado com escalonamento de recurso min-max")
        print("Maior acurácia {}".format(final_accuracy))
        print("Qntd de vezes que a acurácia foi maior que 95% em 100 tentativas: {}\n".format(count_accuracy))
        # print("Acurácia do knn: {}".format(final_accuracy))

        return final_knn


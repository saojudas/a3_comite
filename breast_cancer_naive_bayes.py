#modelo gaussiano
from sklearn import tree
import numpy as np
import pandas as pd
from sklearn.metrics import classification_report
from sklearn.naive_bayes import GaussianNB
from normalizacao import normalizacao
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler
class breast_cancer_naive_bayes():
    final_nb = 0
    normalizacao = normalizacao()

    def treinar_dataset(self,X, y):
        final_accuracy = 0
        count_accuracy=0
        # self.dataset = dataset
        # normalized_dataset = normalizacao.normalizacao_dados(dataset)

         #MODELAGEM DOS DADOS
        scores = []
        i=0
        while i<100:
            #Separação entre dados de teste e de treinamento
            # X_train, X_test, y_train, y_test = train_test_split(normalized_dataset, dataset['target'], test_size=0.30)
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=1, shuffle=True)
            #Instanciando modelo
            nb = GaussianNB()
            #Treinamento
            nb.fit(X_train, y_train)
            precisao = nb.score(X_test, y_test)
            scores.append(precisao)
            #Predições

            predictions = nb.predict(X_test)
            # print(predictions)
            #
            # print (confusion_matrix(y_test, predictions))
            # print(classification_report(y_test,predictions))
            accuracy = accuracy_score(y_test, predictions)
            if(accuracy>0.95):
                count_accuracy=count_accuracy+1
            if (accuracy > final_accuracy and accuracy<1.0):
                final_accuracy=accuracy
                final_nb = nb
                print("Classification report naive bayes")
                print(classification_report(y_test, predictions))
                print("Matriz de confusão naive bayes")
                print(confusion_matrix(y_test, predictions))
            i = i+1
        import matplotlib.pyplot as plt
        import seaborn as sns
        print("Média do Naive Bayes: {:.2f}%".format(np.mean(scores)*100))
        print("Desvio padrão do Naive Bayes: {:.2f}%".format(np.std(scores) * 100))
        print("Naive Bayes - Normalizado com escalonamento de recurso min-max")
        print("Maior acurácia {}".format(final_accuracy))
        print("Qntd de vezes que a acurácia foi maior que 95% em 100 tentativas: {}\n".format(count_accuracy))
        # print("Acurácia do naive bayes: {}".format(final_accuracy))

        return final_nb


from sklearn.metrics import accuracy_score
import joblib
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import confusion_matrix
from normalizacao import normalizacao
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import load_breast_cancer
class teste():
    def treinar_dataset(self, dataset):
        X, y = load_breast_cancer(return_X_y = True)
        X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.10, random_state=1, shuffle=True)
        model = KNeighborsClassifier(n_neighbors=12, weights='distance').fit(X_train, y_train)

        return model